<?php
/*
Plugin Name: cat2post
Plugin URI: https://bitbucket.org/stefanschleifer/wp-cat2post
Description: Link a category to a specific post within this category
Version: 0.1
Author: Stefan Schleifer
Author URI: http://stefanschleifer.com
License: Apache License 2.0 (http://www.apache.org/licenses/LICENSE-2.0.html)
*/

add_action('edit_category_form_fields', 'add_postlink_field');

function add_postlink_field($tag) {
    $t_id = $tag->term_id;
    $cat_meta = get_option( "category_$t_id");
    $saved_id = $cat_meta['extra1'];
?>

<tr class="form-field">
<th scope="row" valign="top"><label for="extra1"><?php _e('Link to article'); ?></label></th>
<td>

<?php query_posts('cat=' . $t_id . '&posts_per_page=-1'); ?>
<select name="Cat_meta[extra1]" id="Cat_meta[extra1]" style="width:60%;">
  <option value="none">- - -</option>
  
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

	<option value="<?php the_ID(); ?>" <?php if(get_the_ID()==$saved_id) { echo "selected='selected'"; } ?>><?php the_title(); ?></option>

<?php endwhile; endif; ?>
</select> 

</td>
</tr>


<?php
}

add_action ('edited_category', 'save_postlink_field');
function save_postlink_field($term_id) {
    if ( isset( $_POST['Cat_meta'] ) ) {
        $t_id = $term_id;
        $cat_meta = get_option( "category_$t_id");
        $cat_keys = array_keys($_POST['Cat_meta']);
            foreach ($cat_keys as $key){
            if (isset($_POST['Cat_meta'][$key])){
                $cat_meta[$key] = $_POST['Cat_meta'][$key];
            }
        }
        update_option( "category_$t_id", $cat_meta );
    }
}


?>
